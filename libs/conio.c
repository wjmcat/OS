#include "conio.h"


static char keycode = -1;


void onGetKeyFunction(char keycode1)
{
	keycode = keycode1;
}
int getch()
{
	char keycode1;
	// printf("onGetKeyFunction:%d\n", &onGetKeyFunction);
    registerListenKey(&onGetKeyFunction);
    while(keycode == -1);
    keycode1 = keycode;
    keycode = -1;
    return keycode1;
}
int getchInStep(uint32_t step)
{
	char keycode1;
	// printf("onGetKeyFunction:%d\n", &onGetKeyFunction);
    registerListenKey(&onGetKeyFunction);
    while(step--);
    keycode1 = keycode;
    keycode = -1;
    return keycode1;
}