
#ifndef INCLUDE_TIMER_H_
#define INCLUDE_TIMER_H_

#include "types.h"

void init_timer(uint32_t frequency);

void register_plane_game_run_time(uint32_t frequency);

void temp_sleep();

#endif 	// INCLUDE_TIMER_H_
