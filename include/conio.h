
#ifndef _CONIO_H
#define _CONIO_H
#include "stdio.h"

int getch();
int getchInStep(uint32_t step);

#endif
