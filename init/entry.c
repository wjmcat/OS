#include "types.h"
#include "common.h"
#include "gdt.h"
#include "idt.h"
#include "timer.h"
#include "stdio.h"
#include "conio.h"
#include "Mcat/command.h"

int kern_entry()
{
    console_clear();
    init_gdt();
    idt_init();
    kb_init();
    command();
    return 0;
}
